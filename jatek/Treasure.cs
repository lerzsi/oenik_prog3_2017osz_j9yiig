﻿namespace Jatek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// A pályán szétszort játékelemek, amiket össze kell gyűjteni
    /// </summary>
    public class Treasure : GameItem
    {
        /// <summary>
        /// A játékelemek kirajzolt geometriájának mérete (szélesség, magasság)
        /// </summary>
        private int[,] treasureSize = { { 50, 40 }, { 40, 40 }, { 40, 40 }, { 40, 30 } };

        /// <summary>
        /// A játékelemek ütközési területének mérete (szélesség, magasság)
        /// </summary>
        private int[,] collideAreaSize = { { 10, 10, 30, 20 }, { 10, 10, 20, 20 }, { 10, 10, 20, 20 }, { 10, 10, 20, 10 } };

        /// <summary>
        /// A kirajzolandó elemek ecsetei
        /// </summary>
        private Brush[] treasureBrushes = new Brush[]
          {
              GetBrush("nagymeteor.png"),
              GetBrush("kismeteor.png"),
              GetBrush("nagyhajo.png"),
              GetBrush("kishajo.png"),
          };

        /// <summary>
        ///  Initializes a new instance of the <see cref="Treasure"/> class.
        /// </summary>
        /// <param name="type">a játékelem típusa</param>
        public Treasure(int type)
        {
            this.Type = type;
            this.CollideArea = new RectangleGeometry(new Rect(this.collideAreaSize[type, 0], this.collideAreaSize[type, 1], this.collideAreaSize[type, 2], this.collideAreaSize[type, 3])).GetFlattenedPathGeometry();
            GeometryGroup group = new GeometryGroup();
            group.Children.Add(new RectangleGeometry(new Rect(this.X, this.Y, this.treasureSize[this.Type, 0], this.treasureSize[this.Type, 1])));
            this.Drawarea = group.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Gets or sets a játékelem értéke
        /// </summary>
        public int Points { get; set; }

        /// <summary>
        /// Gets or sets a játékelem súlya
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// Gets or sets a játékelem típusa
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Gets a játékelem szélessége a típúsának függvényében
        /// </summary>
        public override double Width
        {
            get
            {
                return this.treasureSize[this.Type, 0];
            }
        }

        /// <summary>
        /// Gets a játékelem magassága a típúsának függvényében
        /// </summary>
        public override double Height
        {
            get
            {
                return this.treasureSize[this.Type, 1];
            }
        }

        /// <summary>
        /// Játékelem kirajzolása
        /// </summary>
        /// <param name="drawingContext">a rajz kontextusa</param>
        public override void Draw(DrawingContext drawingContext)
        {
            drawingContext.DrawGeometry(this.treasureBrushes[this.Type], null, this.Drawarea);
        }

        /// <summary>
        /// Egy képet tartalmazó ecsetet hozunk létre
        /// </summary>
        /// <param name="fname">a képfájl neve</param>
        /// <returns>Visszad egy képet tartalmazó ecsetet</returns>
        private static Brush GetBrush(string fname)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(fname, UriKind.Relative)));
            return ib;
        }
    }
}
