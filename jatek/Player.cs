﻿namespace Jatek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Játékos tulajdonságait tartalmazó osztály
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets a játékos aktuális pontjait
        /// </summary>
        public int CurrentPoints { get; set; }

        /// <summary>
        /// Gets or sets az aktuális pályát
        /// </summary>
        public int CurrentLevel { get; set; }

        /// <summary>
        /// Gets or sets hány gyosrítója van a játékosnak
        /// </summary>
        public int SpeedBoostCount { get; set; }

        /// <summary>
        /// Megvizsgálja maradt-e még a játékosnak gyorsítója
        /// </summary>
        /// <returns>maradt-e még a játékosnak gyorsítója</returns>
        public bool UseSpeedBoost()
        {
            if (this.SpeedBoostCount == 0)
            {
                return false;
            }

            this.SpeedBoostCount--;
            return true;
        }
    }
}
