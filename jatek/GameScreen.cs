﻿namespace Jatek
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    /// <summary>
    /// FrameworkElementből származtatott osztály, ami szabályozza a játékot
    /// </summary>
    public class GameScreen : FrameworkElement
    {
        /// <summary>
        /// GameModel típusú példány
        /// </summary>
        private GameModel model;

        /// <summary>
        /// GameLogic típusú példány
        /// </summary>
        private GameLogic logic;

        /// <summary>
        /// Aktuális játékos
        /// </summary>
        private Player currentPlayer = new Player();

        /// <summary>
        /// A szint leírása
        /// </summary>
        private LevelDescription levelDescription = new LevelDescription();

        /// <summary>
        /// Visszaszámláló az idő kiírásához
        /// </summary>
        private int countDown;

        /// <summary>
        /// Időzítő a logis.Ontick metódushoz
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Időzítő az idővisszaszámlálóhoz
        /// </summary>
        private DispatcherTimer timer2;

        /// <summary>
        /// Megállt-e az időzítő
        /// </summary>
        private bool timestop;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScreen"/> class.
        /// </summary>
        public GameScreen()
        {
            this.Loaded += this.GameScreen_Loaded;
        }

        /// <summary>
        /// Az idő lejárta után felugrik egy MessageBox
        /// </summary>
        public void EndGame()
        {
            MessageBoxResult mr;
            if (this.timestop && this.logic.Win)
            {
                mr = MessageBox.Show("You win! Next level!", "Win", MessageBoxButton.OK);
                if (mr == MessageBoxResult.OK)
                {
                    this.NextLevel();
                }
            }
            else
            {
                mr = MessageBox.Show("Game Over! New Game?", "GameOver", MessageBoxButton.YesNo);
                if (mr == MessageBoxResult.Yes)
                {
                    this.StartNewGame();
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }

        /// <summary>
        /// Kirajzolja a képernyőt
        /// </summary>
        /// <param name="drawingContext">rajz kontextus</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.model != null)
            {
                ImageBrush ib = new ImageBrush(new BitmapImage(new Uri("hatter.jpg", UriKind.Relative)));
                FormattedText goal = new FormattedText("Goal: " + this.levelDescription.ReachPoints.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 16, Brushes.White);
                FormattedText currentPoint = new FormattedText("Points: " + this.currentPlayer.CurrentPoints.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 16, Brushes.White);
                FormattedText timeLeft = new FormattedText("Time:  " + this.countDown.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 16, Brushes.White);
                FormattedText speedCount = new FormattedText("Speed:  " + this.currentPlayer.SpeedBoostCount.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 16, Brushes.White);
                drawingContext.DrawGeometry(ib, null, new RectangleGeometry(new Rect(0, 0, ActualWidth, ActualHeight)));
                this.model.Draw(drawingContext);
                drawingContext.DrawText(goal, new Point(5, 5));
                drawingContext.DrawText(currentPoint, new Point(5, 25));
                drawingContext.DrawText(timeLeft, new Point(ActualWidth - 100, 5));
                drawingContext.DrawText(speedCount, new Point(ActualWidth - 100, 25));
            }
        }

        /// <summary>
        /// Mi történjen betöltéskor, ablak példányosítása, timerek példányosítása, beállítása, tick és keydown esemény
        /// </summary>
        /// <param name="sender">az esemény forrása</param>
        /// <param name="e">esemény értéke</param>
        private void GameScreen_Loaded(object sender, RoutedEventArgs e)
        {
            Window akt = Window.GetWindow(this);
            if (akt == null)
            {
                return;
            }

            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(100);
            this.timer.Tick += this.Timer_Tick;
            this.timer2 = new DispatcherTimer();
            this.timer2.Interval = TimeSpan.FromMilliseconds(1000);
            this.timer2.Tick += this.Timer2_Tick;
            this.StartNewGame();
            this.InvalidateVisual();
            akt.KeyDown += this.GameScreen_KeyDown;
        }

        /// <summary>
        /// Tickenként (100 milliszekundum) lefut az logika OnTick metódusa az idő lejártáig
        /// </summary>
        /// <param name="sender">az esemény forrása</param>
        /// <param name="e">esemény értéke</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            this.logic.OnTick();
            if (this.timestop)
            {
                this.timer.Stop();
                this.EndGame();
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// Tickenként (1 másodperc) lefut, visszaszámolja az időt
        /// </summary>
        /// <param name="sender">az esemény forrása</param>
        /// <param name="e">esemény értéke</param>
        private void Timer2_Tick(object sender, EventArgs e)
        {
            this.countDown--;
            if (this.countDown == 0)
            {
                this.timer2.Stop();
                this.timestop = true;
            }
        }

        /// <summary>
        /// Billentyűzet lenyomása
        /// </summary>
        /// <param name="sender">az esemény forrása</param>
        /// <param name="e">esemény értéke</param>
        private void GameScreen_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Down && this.model.Hook.Phase == Hook.Phases.Rotation)
            {
                this.model.Hook.Phase = Hook.Phases.FindTreasure;
            }

            if (e.Key == System.Windows.Input.Key.Up)
            {
                if (this.currentPlayer.SpeedBoostCount > 0 && this.model.Hook.Phase == Hook.Phases.PullUp)
                {
                    this.logic.Speed = true;
                    this.currentPlayer.SpeedBoostCount--;
                }
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// Új játék indítása, szintleírás, játékos, modell és logika példányosítása, időzítők indítása
        /// </summary>
        private void StartNewGame()
        {
            int[,] i = new int[,]
            {
                {
                    0, 20, 4
                },
                {
                    0, 20, 4
                },
                {
                    0, 20, 4
                },
                {
                    1, 10, 1
                },
                {
                    1, 10, 1
                },
                {
                    1, 10, 1
                },
                {
                    1, 10, 1
                },
                {
                    1, 10, 1
                },
                {
                    1, 10, 1
                },
                {
                    1, 10, 1
                },
                {
                    1, 10, 1
                },
                {
                    2, 300, 3
                },
                {
                    2, 300, 3
                },
                {
                    2, 300, 3
                },
                {
                    3, 150, 4
                },
                {
                    3, 150, 4
                },
                {
                    3, 150, 4
                },
                {
                    3, 150, 4
                },
                };
            this.levelDescription = new LevelDescription(
                1000,
                i,
                60,
                3);
            this.model = new GameModel(ActualWidth, ActualHeight, this.levelDescription);
            this.countDown = this.levelDescription.TimeLeft;
            this.currentPlayer = new Player
            {
                CurrentLevel = 1,
                CurrentPoints = 0,
                SpeedBoostCount = this.levelDescription.SpeedBoostCount,
            };

            this.logic = new GameLogic(this.model, this.currentPlayer, this.levelDescription);
            this.timestop = false;
            this.timer.Start();
            this.timer2.Start();
        }

        /// <summary>
        /// Következő pálya betöltése
        /// </summary>
        private void NextLevel()
        {
            this.currentPlayer.CurrentLevel++;
            this.levelDescription.ReachPoints = 1000 * this.currentPlayer.CurrentLevel;
            this.model = new GameModel(ActualWidth, ActualHeight, this.levelDescription);
            this.currentPlayer.SpeedBoostCount = this.levelDescription.SpeedBoostCount;
            this.countDown = this.levelDescription.TimeLeft;
            this.logic = new GameLogic(this.model, this.currentPlayer, this.levelDescription);
            this.timestop = false;
            this.timer.Start();
            this.timer2.Start();
        }
    }
}
