﻿namespace Jatek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Jatekelemek leirasat tartalmazo osztaly
    /// </summary>
    public abstract class GameItem
    {
        /// <summary>
        /// Elforgatás szöge fokban
        /// </summary>
        protected double degree;

        /// <summary>
        /// A jelenlegi transzformációk
        /// </summary>
        protected TransformGroup currentTransformation = new TransformGroup();

        /// <summary>
        /// A kirajzolt geometria.
        /// </summary>
        private Geometry drawarea;

        /// <summary>
        /// A geometria amelyikkel az ütközéseket vizsgáljuk
        /// </summary>
        private Geometry collideArea;

        /// <summary>
        /// A geometria X koordinátája
        /// </summary>
        private double x = 0;

        /// <summary>
        /// A geometria Y koordinátája
        /// </summary>
        private double y = 0;

        /// <summary>
        /// Gets geometria szélessége
        /// </summary>
        public abstract double Width { get; }

        /// <summary>
        /// Gets geometria magassága
        /// </summary>
        public abstract double Height { get; }

        /// <summary>
        /// Gets or sets elforgatás szöge fokban
        /// </summary>
        public virtual double Degree
        {
            get
            {
                return this.degree;
            }

            set
            {
                if (this.degree == value)
                {
                    return;
                }

                this.currentTransformation.Children.Add(new RotateTransform(value - this.degree, this.X + (this.Width / 2), this.Y + (this.Height / 2)));
                this.degree = value;
            }
        }

        /// <summary>
        /// Gets or sets az elforgatás szög radiánban
        /// </summary>
        public double Rad
        {
            get
            {
                return this.Degree * Math.PI / 180;
            }

            set
            {
                this.Degree = 180 * value / Math.PI;
            }
        }

        /// <summary>
        /// Gets or sets a geometria X koordinátája
        /// </summary>
        public double X
        {
            get
            {
                return this.x;
            }

            set
            {
                if (this.x == value)
                {
                    return;
                }

                this.currentTransformation.Children.Add(new TranslateTransform(value - this.X, 0));
                this.x = value;
            }
        }

        /// <summary>
        /// Gets or sets a geometria Y koordinátája
        /// </summary>
        public double Y
        {
            get
            {
                return this.y;
            }

            set
            {
                if (this.y == value)
                {
                    return;
                }

                this.currentTransformation.Children.Add(new TranslateTransform(0, value - this.Y));
                this.y = value;
            }
        }

        /// <summary>
        /// Gets or sets a kirajzolt geometria.
        /// </summary>
        public Geometry Drawarea
        {
            get
            {
                if (this.currentTransformation.Children.Count > 0)
                {
                    this.FinishTransformation();
                }

                return this.drawarea;
            }

            set
            {
                this.drawarea = value;
            }
        }

        /// <summary>
        /// Gets or sets a geometria amelyikkel az ütközéseket vizsgáljuk
        /// </summary>
        public Geometry CollideArea
        {
            get
            {
                if (this.currentTransformation.Children.Count > 0)
                {
                    this.FinishTransformation();
                }

                return this.collideArea;
            }

            set
            {
                this.collideArea = value;
            }
        }

        /// <summary>
        /// Két geometria ütközését vizsgáljuk.
        /// </summary>
        /// <param name="thisGeometry">Az egyik geometria, amelyiknek az ütközését vizsgáljuk.</param>
        /// <param name="collidesWith">A másik geometria, amelyiikel vizsgáljuk az ütközést</param>
        /// <returns>Visszaadja, hogy a két paraméterként megadott geometria ütközik-e egymással.</returns>
        public bool IsCollide(Geometry thisGeometry, Geometry collidesWith)
        {
            return Geometry.Combine(thisGeometry, collidesWith, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }

        /// <summary>
        /// A leszármazott osztályok számára megkövetelt metódus, ami a rajzolást fogja végezni.
        /// </summary>
        /// <param name="drawingContext">Kirajzolandó kontextus</param>
        public abstract void Draw(DrawingContext drawingContext);

        /// <summary>
        /// Az aktuális transzformációkat elvégző metódus.
        /// </summary>
        private void FinishTransformation()
        {
            TransformGroup tg = this.currentTransformation.Clone();
            this.currentTransformation.Children.Clear();
            if (this.collideArea != null)
            {
                this.collideArea.Transform = tg;
                this.collideArea = this.collideArea.GetFlattenedPathGeometry();
            }

            if (this.Drawarea != null)
            {
                this.drawarea.Transform = tg;
                this.drawarea = this.drawarea.GetFlattenedPathGeometry();
            }
        }
    }
}
