﻿namespace Jatek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Kirajzolandó játékelemek és azok elhelyezése
    /// </summary>
    public class GameModel
    {
        /// <summary>
        /// Hook játékelem
        /// </summary>
        private Hook hook;

        /// <summary>
        /// Treasure játékelemeket tartalmazó lista
        /// </summary>
        private List<Treasure> treasures = new List<Treasure>();

        /// <summary>
        /// Random számgenerátor
        /// </summary>
        private Random r = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="width">az ablak szélessége</param>
        /// <param name="height">az ablak magassága</param>
        /// <param name="description">szint leírás</param>
        public GameModel(double width, double height, LevelDescription description)
        {
            this.Hook = new Hook();
            this.Hook.ScreenWidth = width;
            this.Hook.ScreenHeight = height;

            for (int descriptionRow = 0; descriptionRow < description.TreasureDescription.GetLength(0); descriptionRow++)
            {
                Treasure generatedTreasure = new Treasure(description.TreasureDescription[descriptionRow, 0]);
                generatedTreasure.Points = description.TreasureDescription[descriptionRow, 1];
                generatedTreasure.Weight = description.TreasureDescription[descriptionRow, 2];
                bool isCollide;
                do
                {
                    isCollide = false;
                    this.MoveToRandomPosition(generatedTreasure, 0, (int)width - 50, 150, (int)height - 40);
                    int i = 0;
                    while (i < this.Treasures.Count && !isCollide)
                    {
                        Treasure addedTreasure = this.Treasures[i];
                        if (generatedTreasure.IsCollide(generatedTreasure.Drawarea, addedTreasure.Drawarea))
                        {
                            isCollide = true;
                        }

                        i++;
                    }
                }
                while (isCollide);
                this.Treasures.Add(generatedTreasure);
            }
        }

        /// <summary>
        /// Gets or sets hook játékelem
        /// </summary>
        public Hook Hook
        {
            get
            {
                return this.hook;
            }

            set
            {
                this.hook = value;
            }
        }

        /// <summary>
        /// Gets or sets treasure játékelemeket tartalmazó lista
        /// </summary>
        public List<Treasure> Treasures
        {
            get
            {
                return this.treasures;
            }

            set
            {
                this.treasures = value;
            }
        }

        /// <summary>
        /// Hook és treasure játékelemek kirajzolása
        /// </summary>
        /// <param name="drawingContext">a rajz kontextusa</param>
        public void Draw(DrawingContext drawingContext)
        {
            Hook.Draw(drawingContext);
            foreach (Treasure treasure in this.Treasures)
            {
                treasure.Draw(drawingContext);
            }
        }

        /// <summary>
        /// Random pozícióba helyezi a treasure játékelemeket
        /// </summary>
        /// <param name="treasure">A treasure játékelem, aminek koordinátát osztunk</param>
        /// <param name="fromX">minimum x koordináta</param>
        /// <param name="untilX">maximum x koordináta</param>
        /// <param name="fromY">minimum y koordináta</param>
        /// <param name="untilY">maximum y koordináta</param>
        private void MoveToRandomPosition(Treasure treasure, int fromX, int untilX, int fromY, int untilY)
        {
            treasure.X = this.r.Next(fromX, untilX);
            treasure.Y = this.r.Next(fromY, untilY);
        }
    }
}
