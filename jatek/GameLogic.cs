﻿namespace Jatek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// A képernyőn történő mozgásokat kezelő osztály
    /// </summary>
    public class GameLogic
    {
        /// <summary>
        /// GameModel típusú példány
        /// </summary>
        private GameModel model;

        /// <summary>
        /// A rotáció iránya jobbra tart-e
        /// </summary>
        private bool hookFishingRight = false;

        /// <summary>
        /// Játékos példány
        /// </summary>
        private Player player;

        /// <summary>
        /// Szintleírás példány
        /// </summary>
        private LevelDescription levelDescription;

        /// <summary>
        /// Gyorsítás adtunk-e
        /// </summary>
        private bool speed;

        /// <summary>
        /// Elkapott treasure játékelem
        /// </summary>
        private Treasure hookTreasure;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="model">model átadása</param>
        /// <param name="player">játékos átadása</param>
        /// <param name="leveldescription">szintleírás átadása</param>
        public GameLogic(GameModel model, Player player, LevelDescription leveldescription)
        {
            this.model = model;
            this.player = player;
            this.levelDescription = leveldescription;
        }

        /// <summary>
        /// Gets a value indicating whether nyert-e a játékos
        /// </summary>
        public bool Win { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether gyorsítást adtunk-e
        /// </summary>
        public bool Speed
        {
            get
            {
                return this.speed;
            }

            set
            {
                this.speed = value;
            }
        }

        /// <summary>
        /// A tickenként meghívott metódus
        /// </summary>
        public void OnTick()
        {
            this.CheckLevelConditions();
            if (this.model.Hook.Phase == Hook.Phases.Rotation)
            {
                this.DoFishing();
            }

            if (this.model.Hook.Phase == Hook.Phases.FindTreasure)
            {
                this.FindHookStart();
            }

            if (this.model.Hook.Phase == Hook.Phases.PullUp)
            {
                this.DoPullUp();
            }

            if (this.model.Hook.Phase == Hook.Phases.PullUpEnd)
            {
                this.DoPullUpEnd();
            }
        }

        /// <summary>
        /// Megvizsgálja hogy a pálya teljesítéséhez szükséges pontok összegyűltek-e
        /// </summary>
        public void CheckLevelConditions()
        {
            if (this.player.CurrentPoints >= this.levelDescription.ReachPoints)
            {
                this.Win = true;
            }
        }

        /// <summary>
        /// A hook játékelem rotációját végző metódus.
        /// </summary>
        private void DoFishing()
        {
            if (this.model.Hook.Degree >= 84)
            {
                this.hookFishingRight = true;
            }

            if (this.model.Hook.Degree <= -84)
            {
                this.hookFishingRight = false;
            }

            if (!this.hookFishingRight)
            {
                this.model.Hook.Degree += this.model.Hook.RotationSpeed;
            }
            else
            {
                this.model.Hook.Degree -= this.model.Hook.RotationSpeed;
            }
        }

        /// <summary>
        /// Mvizsgálja, hogy van-e olyan treasure játékelem, amit fel tud szedni, ha van a hookTreasurebe helyezi.
        /// </summary>
        private void FindHookStart()
        {
            this.hookTreasure = this.model.Hook.FindTreasureToPickUp(this.model.Treasures);
            this.model.Hook.Phase = Hook.Phases.PullUp;
        }

        /// <summary>
        /// A hookTreasure felhúzását végző metódus.
        /// </summary>
        private void DoPullUp()
        {
            double speedModifier = 1;

            if (this.hookTreasure != null)
            {
                speedModifier = 10 / (10 - this.hookTreasure.Weight);
                this.model.Hook.PullSpeedUp = 5 * speedModifier;
                if (this.Speed)
                {
                    this.model.Hook.PullSpeedUp = this.model.Hook.PullSpeedUp * 3;
                }

                if (this.hookTreasure.Y > this.model.Hook.Y)
                {
                    this.hookTreasure.X = this.hookTreasure.X + (this.model.Hook.PullSpeedUp * Math.Sin(this.model.Hook.Rad));
                    this.hookTreasure.Y = this.hookTreasure.Y - (this.model.Hook.PullSpeedUp * Math.Cos(this.model.Hook.Rad));
                }
                else
                {
                    this.Speed = false;
                    this.model.Hook.Phase = Hook.Phases.PullUpEnd;
                }
            }
            else
            {
                this.model.Hook.Phase = Hook.Phases.PullUpEnd;
            }
        }

        /// <summary>
        /// Ha felért a hookTreasure a játékos kapja meg a hookTreasure értékét, és induljon újra a rotáció
        /// </summary>
        private void DoPullUpEnd()
        {
            if (this.hookTreasure != null)
            {
                this.player.CurrentPoints += this.hookTreasure.Points;
                this.model.Treasures.Remove(this.hookTreasure);
                this.hookTreasure = null;
            }

            this.model.Hook.HookFishing = null;
            this.model.Hook.Phase = Hook.Phases.Rotation;
        }
    }
}
