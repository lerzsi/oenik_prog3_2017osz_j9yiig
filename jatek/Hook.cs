﻿namespace Jatek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// A középen forgó játékelem
    /// </summary>
    public class Hook : GameItem
    {
        /// <summary>
        /// Az a geometria ami, célzás után kirajzolódik
        /// </summary>
        private Geometry hookFishing;

        /// <summary>
        /// A játékelem kezdeti X koordinátája
        /// </summary>
        private double startX;

        /// <summary>
        /// A játékelem kezdeti Y koordinátája
        /// </summary>
        private double startY;

        /// <summary>
        /// A játékelem aktuális fázisa
        /// </summary>
        private Phases phase;

        /// <summary>
        /// Initializes a new instance of the <see cref="Hook"/> class.
        /// </summary>
        public Hook()
        {
            this.Phase = Phases.Rotation;
        }

        /// <summary>
        /// A jéték különböző fázisai
        /// </summary>
        public enum Phases
        {
            /// <summary>
            /// A célzó játékelem balra-jobbra forog
            /// </summary>
            Rotation,

            /// <summary>
            /// Célzás fázisa
            /// </summary>
            FindTreasure,

            /// <summary>
            /// A talált játékelem felhúzásának fázisa
            /// </summary>
            PullUp,

            /// <summary>
            /// A felhúzás befejetésének fázisa
            /// </summary>
            PullUpEnd
        }

        /// <summary>
        /// Gets or sets a felhúzás sebessége
        /// </summary>
        public double PullSpeedUp { get; set; }

        /// <summary>
        /// Gets a rotáció sebessége
        /// </summary>
        public double RotationSpeed
        {
            get
            {
                return 5;
            }
        }

        /// <summary>
        /// Gets or sets a képernyő szélessége
        /// </summary>
        public double ScreenWidth { get; set; }

        /// <summary>
        /// Gets or sets a képernyő magassága
        /// </summary>
        public double ScreenHeight { get; set; }

        /// <summary>
        /// Gets a játélkelem szélessége
        /// </summary>
        public override double Width
        {
            get
            {
                return 20;
            }
        }

        /// <summary>
        /// Gets a játékelem magassága
        /// </summary>
        public override double Height
        {
            get
            {
                return 10;
            }
        }

        /// <summary>
        /// Sets a játékelem elforgatási szöge
        /// </summary>
        public override double Degree
        {
            set
            {
                if (this.degree == value)
                {
                    return;
                }

                this.currentTransformation.Children.Add(new RotateTransform(value - this.degree, this.X + (this.Width / 2), this.Y));
                this.degree = value;
            }
        }

        /// <summary>
        /// Gets or sets a geometria ami, célzás után kirajzolódik
        /// </summary>
        public Geometry HookFishing
        {
            get
            {
                return this.hookFishing;
            }

            set
            {
                this.hookFishing = value;
            }
        }

        /// <summary>
        /// Gets or sets a játékelem aktuális fázisa
        /// </summary>
        internal Phases Phase
        {
            get
            {
                return this.phase;
            }

            set
            {
                this.phase = value;
            }
        }

        /// <summary>
        /// A játékelem kirajzolása hogyan történjen.
        /// </summary>
        /// <param name="drawingContext"> a rajz kontextusa</param>
        public override void Draw(DrawingContext drawingContext)
        {
            if (this.Drawarea == null)
            {
                this.Drawarea = new RectangleGeometry(new Rect(this.X, this.Y, this.Width, this.Height)).GetFlattenedPathGeometry();

                this.Y = 50;
                this.X = (this.ScreenWidth / 2) - (this.Width / 2);
                this.startX = this.X;
                this.startY = this.Y;
            }

            if (this.HookFishing != null)
            {
                drawingContext.DrawGeometry(
                    Brushes.Yellow,
                    new Pen(Brushes.White, 1),
                    this.HookFishing);
            }

            drawingContext.DrawGeometry(
                    Brushes.Aqua,
                   new Pen(Brushes.Black, 2),
                   this.Drawarea);
        }

        /// <summary>
        /// Megkeresi hogy a becélzott területen van-e olyan elem, amit fel tudunk szedni
        /// </summary>
        /// <param name="treasures"> A pályán találhato felszedendő játékelemek</param>
        /// <returns>Visszadaja a legközelebbi felszedhető elemet, vagy ha nincs ilyen elem akkor nullt</returns>
        public Treasure FindTreasureToPickUp(List<Treasure> treasures)
        {
            List<Treasure> collidesWith = new List<Treasure>();

            RectangleGeometry pickUpCollideArea = new RectangleGeometry(new Rect(this.X, this.Y, this.Width, this.ScreenHeight * 2));
            TransformGroup tg = new TransformGroup();
            tg.Children.Add(new RotateTransform(this.Degree, this.X + (this.Width / 2), this.Y));

            pickUpCollideArea.Transform = tg;
            this.HookFishing = pickUpCollideArea;
            foreach (Treasure treasure in treasures)
            {
                if (treasure.IsCollide(treasure.CollideArea, pickUpCollideArea))
                {
                    collidesWith.Add(treasure);
                }
            }

            if (collidesWith.Count > 0)
            {
                Treasure closestTreasure = null;
                double minDistance = -1;
                foreach (Treasure collidedTreasure in collidesWith)
                {
                    double distance = Math.Sqrt(Math.Pow(collidedTreasure.X - this.X, 2) + Math.Pow(collidedTreasure.Y - this.Y, 2));
                    if (distance < minDistance || minDistance == -1)
                    {
                        minDistance = distance;
                        closestTreasure = collidedTreasure;
                    }
                }

                return closestTreasure;
            }

            return null;
        }
    }
}