﻿namespace Jatek
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A szint tulajdonságait tartalmazó osztály
    /// </summary>
    public class LevelDescription
    {
        /// <summary>
        ///  Initializes a new instance of the <see cref="LevelDescription"/> class.
        /// </summary>
        /// <param name="reachPoints">elérendő pontszám</param>
        /// <param name="treasureDescription">pályán lévő treasure játékelemes listája</param>
        /// <param name="timeLeft">idő ami alatt meg kell szerezni a szükséges pont</param>
        /// <param name="speedBoostCount">gyorsítók darabszáma</param>
        public LevelDescription(int reachPoints, int[,] treasureDescription, int timeLeft, int speedBoostCount)
        {
            this.ReachPoints = reachPoints;
            this.TreasureDescription = treasureDescription;
            this.TimeLeft = timeLeft;
            this.SpeedBoostCount = speedBoostCount;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LevelDescription"/> class.
        /// </summary>
        public LevelDescription()
        {
        }

        /// <summary>
        /// Gets or sets a pálya teljesítéséhet szükséges pontot
        /// </summary>
        public int ReachPoints { get; set; }

        /// <summary>
        /// Gets a pályán található treasure játékelemek (típus, pont, súly)
        /// </summary>
        public int[,] TreasureDescription { get; }

        /// <summary>
        /// Gets mennyi idő van a pálya teljesítésére
        /// </summary>
        public int TimeLeft { get; }

        /// <summary>
        /// Gets or sets gyorsítók darabszáma
        /// </summary>
        public int SpeedBoostCount { get; set; }
    }
}
